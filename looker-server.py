# -*- coding: utf-8 -*-

import socket
import sys
import time
import Adafruit_PCA9685

# Częstotliwość sterowania PWM
FREQUENCY = 50
# Czas jednego cyklu
CYCLE_TIME = 1 / float(FREQUENCY)
# Procentowy czas cyklu dla położenia minimalnego
SERVO_MIN_PERCENT = 0.00055 / CYCLE_TIME
# Procentowy czas cyklu dla położenia maksymalnego
SERVO_MAX_PERCENT = 0.00245 / CYCLE_TIME
# Wartość minimalna dla 4096 kroków
SERVO_MIN = int(4096 * SERVO_MIN_PERCENT)
# Wartość maksymalna dla 4096 kroków
SERVO_MAX = int(4096 * SERVO_MAX_PERCENT)    

# Tworzymy obiekt pwm (przyjmie domyślnie adres 0x40)
pwm = Adafruit_PCA9685.PCA9685()
pwm.set_pwm_freq(FREQUENCY)

def setAngle(channel, angle):
    pwm.set_pwm(channel, 0, int(SERVO_MIN + (float(angle) / 180.0) * (SERVO_MAX - SERVO_MIN)))

def processCommand(command):
    angles = command.split("|")
    angles = list(map(lambda x: max(-90, min(90, -int(x.strip()))) + 90, angles))

    print("P: {0}, Y: {1}".format(angles[0], angles[2]))
    setAngle(14, int(angles[0] / 10) * 10)
    setAngle(15, int(angles[2] / 10) * 10)

def handleConnection(connection):
    buffer = ""

    while True:
        data = connection.recv(32)
        if data:
            buffer = buffer + data
            commandEnd = buffer.find(";")
            if (commandEnd != -1):
                command = buffer[0:commandEnd]
                buffer = buffer[commandEnd+1:]
                processCommand(command)
                
        else:
            break

    # Resetting servos
    setAngle(14, 90)
    setAngle(15, 90)


def main():

    # Create TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = ('', 10000)
    sock.bind(server_address)

    # Listen for incomming connections
    sock.listen(10)

    try:

        while True:
            print('Waiting for connection...')
            connection, client_address = sock.accept()
            
            try:
                print('Connection accepted from: ', client_address)

                handleConnection(connection)

            finally:
                print('Closing connection...')
                connection.close()
            
    except KeyboardInterrupt:
        print('Ctrl+C received, closing...')

# Start program
main()